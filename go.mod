module go.octolab.org/toolkit/cli

go 1.13

require (
	github.com/golang/mock v1.4.4
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	go.octolab.org v0.9.0
	go.octolab.org/toolkit/config v0.0.4
)
